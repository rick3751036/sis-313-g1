import styles from "./page.module.css";
import Plantilla from "@/component/Plantilla";
import { Button } from "@/component/Button";
import Videos from "@/component/Videos";
import PlanCuentas from "@/component/PlanCuentas";

export default function Home() {
  return (
    <main className={styles.main}>
        <div className="container">
          <div>
            <Button title="HOLA" ></Button>
            <Button title="ADIOS" transparent></Button>
          </div>
          <div className="tarjetas">
            <Plantilla title="Psyduck" img="/imagen/Psyduck.png" tipe="Agua"></Plantilla>
            <Plantilla title="Pikachu" img="/imagen/pikachu.jpg" tipe="Eléctrico"></Plantilla>
            <Plantilla title="Infernape" img="/imagen/infernape.jpg" tipe="Fuego"></Plantilla>
            <Plantilla title="Serperior" img="/imagen/Serperior.png" tipe="Planta"></Plantilla>
            <Plantilla title="Gengar" img="/imagen/gengar.jpg" tipe="Fantasma"></Plantilla>
          </div>
          <div className="videos">
            <Videos title="Me Enteré" name="Tiago PZK, TINI" video="/videos/video1.mp4"></Videos>
            <Videos title="QUE SED" name="Luck Ra, Ulises Bueno" video="/videos/video2.mp4"></Videos>
            <Videos title="MOJABI GHOST" name="Tainy, BAD BUNNY " video="/videos/video3.mp4"></Videos>
          </div>
          <div className="tarjeta-plan">
            <PlanCuentas imagen="/imagen/plan-img.svg" title="Free Plan" parrafo="Unlimited Bandwitch" precio="Free"></PlanCuentas>
            <PlanCuentas imagen="/imagen/logo.svg" title="Free Plan" parrafo="Unlimited Bandwitch" precio="$9"></PlanCuentas>
            <PlanCuentas imagen="/imagen/baner-img-2.svg" title="Free Plan" parrafo="Unlimited Bandwitch" precio="$12"></PlanCuentas>
          </div>
        </div>
    </main>
  );
}
