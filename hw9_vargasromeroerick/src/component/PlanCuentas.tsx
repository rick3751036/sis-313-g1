import FeatherIcon from 'feather-icons-react';
import Red from './Button-red';

export default function PlanCuentas(props:{imagen:string,title:string,parrafo:string,precio:string}){
    return(
        <div className="plan">
            <img src={props.imagen} alt="" />
            <h2>{props.title}</h2>
            <ul>
                <li><FeatherIcon icon="check" />Unlimited Bandwitch</li>
                <li><FeatherIcon icon="check" />Encrypted Connection</li>
                <li><FeatherIcon icon="check" />No Traffic Logs</li>
                <li><FeatherIcon icon="check" />Works on All Devices</li>
            </ul>
            <h1>{props.precio}/ mo</h1>
            <Red>select</Red>
        </div>
    )
}