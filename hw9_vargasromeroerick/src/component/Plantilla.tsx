export default function Plantilla(prop:{title:string,img:string,tipe:string}){
    return(
        <div className="cuadrado">
            <h1>{prop.title}</h1>
            <img src={prop.img} alt="" />
            <h3>tipo : {prop.tipe}</h3>
        </div>
    )
}