export default function Videos(props:{title:string,video:any,name:string}){
    return(
        <div className="vd">
            <h1>{props.title}</h1>
            <h3>Artista : {props.name}</h3>
            <video src={props.video} controls></video>
        </div>
    )
}