import Logo from "@/components/Logo"
import Header from "@/components/Header"
import Title from "@/components/Title"
import Input from "@/components/Input"
import Images from "@/components/Images-login"

const Sign = ()=>{
    return(
        <div className="container registre">
            <Logo end></Logo>
            <div className="sign-up">
                <div className="image-registre">
                    <Images imgUrl="/images/login2.svg"></Images>
                    <Images imgUrl="/images/login2.svg"></Images>
                    <Images imgUrl="/images/login2.svg"></Images>
                </div>
                <div>
                <Title title="Sign up" pagram="Let’s get you all st up so you can access your personal account."></Title>
                <div className="nombre">
                    <Input text="text" secondary subtitle="First Name"></Input>
                    <Input text="text" secondary subtitle="Last Name"></Input>
                </div>
                <div className="nombre">
                    <Input text="text" secondary subtitle="Email"></Input>
                    <Input text="text" secondary subtitle="Phone Number"></Input>
                </div>
                <Input text="password" subtitle="Password"></Input>
                <Input text="password" subtitle="Confirm Password"></Input>
                <Header subtitle="Already have an account? " enlazar="Login" pagram="Sign up"></Header>
                </div>
            </div>
        </div>
    )
}
export default Sign