import Image from "../../node_modules/next/image";
import styles from "./page.module.css";
import Logo from "@/components/Logo";
import Images from "@/components/Images-login";
import Title from "@/components/Title";
import Header from "@/components/Header";
import Input from "@/components/Input";

export default function Home() {
  return (
    <main className={styles.main}>
      <section className="container2 principal">
      <Logo></Logo>
      <div className="container2 secundari">
        <div>
          <Title title="Login" pagram="Login to access your travelwise  account"></Title>
          <Input text="text" subtitle="Email"></Input>
          <Input text="password" subtitle="Password"></Input>
          <Header subtitle="Don’t have an account? " enlazar="Sign up" pagram="login" login ></Header>
        </div>
        <div className="login-viwer">
          <Images imgUrl="/images/login1.svg"></Images>
          <Images imgUrl="/images/login1.svg"></Images>
          <Images imgUrl="/images/login1.svg"></Images>
        </div>
      </div>
      </section>
    </main>
  );
}
