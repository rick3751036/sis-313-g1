import Image from "../../node_modules/next/image"

export default function Button(props:{title:string,transparent?:boolean,imgUrl:string}){
    return(
        props.transparent?
        <button className="btn-transparent"><Image src={props.imgUrl} alt="" width={100} height={100}></Image></button>
        :
        <button className="btn-primary">{props.title}</button>
    )
}