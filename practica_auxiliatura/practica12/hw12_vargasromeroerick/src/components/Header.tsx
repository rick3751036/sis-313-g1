import Button from "./Button"
import Link from "../../node_modules/next/link"
import Input from "./Input"
import Check from "./Input-check"

export default function Header(props:{subtitle:string,enlazar:string,login?:boolean,enlazar2?:string,subtitle2?:string,pagram:string}){
    return(
        props.login?
        <div className="header">
            <div className="check">
                <Check text="checkbox" subtitle="Remember me"></Check>
            </div>
            <Button title="Login" imgUrl=""></Button>
            <div className="recuerda">
                <h4>{props.subtitle}</h4>
                <Link href="/Sign-up" className='active'>{props.enlazar}</Link>
            </div>
            <h4>Or {props.pagram} with</h4>
            <div className="service">
                <Button title="" imgUrl="/images/facebook.svg" transparent></Button>
                <Button title="" imgUrl="/images/google.svg" transparent></Button>
                <Button title="" imgUrl="/images/apple.svg" transparent></Button>
            </div>
        </div>
        :
        <div className="header">
        <div className="check">
            <Check text="checkbox" subtitle="I agree to all the Terms and Privacy Policies"></Check>
        </div>
        <Button title="Create account" imgUrl=""></Button>
        <div className="recuerda">
            <h4>{props.subtitle}</h4>
            <Link href="/" className='active'>{props.enlazar}</Link>
        </div>
        <h4>Or {props.pagram} with</h4>
        <div className="service">
            <Button title="" imgUrl="/images/facebook.svg" transparent></Button>
            <Button title="" imgUrl="/images/google.svg" transparent></Button>
            <Button title="" imgUrl="/images/apple.svg" transparent></Button>
        </div>
    </div>
    
    )
}