import Image from "../../node_modules/next/image"

export default function Images(props:{imgUrl:string}){
    return(
        <div className="login-content">
            <Image src={props.imgUrl} alt="" width={100} height={100}></Image>
        </div>
    )
}