export default function Check(props:{text:string,subtitle?:string}){
    return(
        <div className="top-input">
            <input type={props.text}  className="input-secondary"/>
            <label>{props.subtitle}</label>
        </div>
        
        
    )
}