export default function Input(props:{text:string,secondary?:boolean,subtitle?:string}){
    return(
        props.secondary?
        <div className="top-input">
            <input type={props.text}  className="input-secondary"/>
            <label className="label">{props.subtitle}</label>
        </div>
        :
        <div className="top-input">
            <input type={props.text} className="input-primary"/>
            <label className="label">{props.subtitle}</label>
        </div>
        
        
    )
}