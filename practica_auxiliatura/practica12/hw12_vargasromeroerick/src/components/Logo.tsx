export default function Logo(props:{end?:boolean}){
    return(
        props.end?
        <div className="reverse">
            <i className="icon icon-logo"></i>
            <span>Your Logo</span>
        </div>
        :
        <div className="logo-principal">
            <i className="icon icon-logo"></i>
            <span>Your Logo</span>
        </div>
    )
}
