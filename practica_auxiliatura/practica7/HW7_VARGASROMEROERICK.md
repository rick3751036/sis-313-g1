# VARGAS ROMERO ERICK

**yo en principio utilice los comandos**

**cd para salir de las carpetas predeterminadas**

**dir para mostrar las carpetas que tenia**

**mkdir para crear la carpeta**

**y nuevamente dir para comprobar que se creo correctamente la carpetaS**



# 5 comandos para las terminales de windows y linux

# PARA WINDOWS

**cd: Sirve para cambiar de directorio**

**dir: El comando lista el contenido del directorio o carpeta donde te encuentras**

**mkdir: Crea una carpeta con el nombre que le asignes en la dirección en la que te encuentres en ese momento**

**rename : Renombra archivos**

**del: Elimina archivos o contenido de carpetas.**

# PARA LINUX

**ls: te permite listar el contenido del directorio que quieras**

**cd: Sirve para cambiar de directorio**

**mv: se utiliza para mover (o renombrar) archivos y directorios en el sistema de archivos**

**mkdir: Crea una carpeta con el nombre que le asignes en la dirección en la que te encuentres en ese momento**

**unzip: permite extraer el contenido de un archivo .zip desde el terminal**